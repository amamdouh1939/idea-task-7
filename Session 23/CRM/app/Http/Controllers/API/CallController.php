<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Call;

class CallController extends Controller
{
    public function index()
    {
        $calls = Call::all();

        return $calls;
    }

    public function show(Call $call){
        return $call;
    }
}
