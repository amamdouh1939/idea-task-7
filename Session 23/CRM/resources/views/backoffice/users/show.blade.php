@extends('layouts.app')

@section('title', 'Show User')

@section('pagename', 'Show User')

@section('content')
    <h2>More information about {{ $user->name }}</h2>
    <hr>
    <h3><strong>Name: </strong> {{ $user->name }}</h3>
    <h3><strong>Email: </strong>{{ $user->email }}</h3>
    <h3><strong>Role: </strong>{{ $user->role->title }}</h3>
    @if($user->role->slug == "SALES")
        <hr>
        @if(sizeof($user->leads) > 0)
            <h3><strong>Associated Leads</strong></h3>
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                </thead>
                <tbody>
                    @foreach($user->leads as $lead)
                        <tr>
                            <td>{{ $lead->name }}</td>
                            <td>{{ $lead->email }}</td>
                            <td>{{ $lead->phone }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h3><strong>{{ $user->name }} currently has no associated leads</strong></h3>
        @endif
    @endif
    <div>
        @if(Auth::user()->role->slug == "ADMIN")
            <hr>
            <a href="{{ route('users.edit', $user) }}" class="btn btn-warning">Edit</a>
        @endif
        @if(Auth::user()->role->slug == "ADMIN")
            <form style="display: inline-block" action="{{ route('users.destroy', $user) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        @endif
    </div>

@endsection
