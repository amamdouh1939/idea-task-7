@extends('layouts.app')

@section('title', 'Leads')
@section('pagename', 'View All Leads')

@section('content')
@if(Auth::user()->role->slug === "ADMIN")
    <a href="{{ route('leads.create') }}" class="btn btn-success">Add New</a>
    <hr>
@endif

@if(Auth::user()->role->slug == 'ADMIN' || Auth::user()->role->slug == "TMLDR")
<table class="table table-bordered">
    <thead>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Salesperson</th>
        <th>Actions</th>
    </thead>
        <tbody>
            @foreach($leads as $lead)
                <tr>
                    <td>{{ $lead->name }}</td>
                    <td>{{ $lead->phone }}</td>
                    <td>{{ $lead->email }}</td>
                    <td>
                        <form action="{{ route('leads.assign', $lead) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <select name="sales" id="sales" class="form-control">
                                <option value="0">Unassigned</option>
                                @foreach($sales as $salesperson)
                                    @if($lead->sales && $lead->sales->id === $salesperson->id)
                                        <option value="{{ $salesperson->id }}" selected>{{ $salesperson->name }}</option>
                                    @else
                                        <option value="{{ $salesperson->id }}"> {{ $salesperson->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <div class="text-center m-2">
                                <button type="submit" class="btn btn-primary">Assign</button>
                            </div>
                        </form>
                    </td>
                    <td>
                        <a href="{{ route('leads.show', $lead) }}" class="btn btn-primary">Show</a>
                        @if(Auth::user()->role->slug === "ADMIN")
                            <a href="{{ route('leads.edit', $lead)}}" class="btn btn-warning">Edit</a>
                        @endif

                        @if(Auth::user()->role->slug == "ADMIN")
                            <form style="display: inline-block" action="{{ route('leads.destroy', $lead) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        @endif
                        @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                            <a href="{{ route('calls.create', $lead) }}" class="btn btn-success">Call</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
        {{ $leads->links() }}
</table>
@elseif(Auth::user()->role->slug == "SALES")
@if(sizeof(Auth::user()->leads) > 0)
    <table class="table table-bordered">
        <thead>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Actions</th>
        </thead>
            <tbody>
                @foreach(Auth::user()->leads as $lead)
                    <tr>
                        <td>{{ $lead->name }}</td>
                        <td>{{ $lead->phone }}</td>
                        <td>{{ $lead->email }}</td>
                        <td>
                            @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                                <a href="{{ route('calls.create', $lead) }}" class="btn btn-success">Call</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
            {{ $leads->links() }}
    </table>

@else
    <h3><strong>There are no assigned leads for {{ Auth::user()->name }}.</strong></h3>
@endif
@endif
@endsection
