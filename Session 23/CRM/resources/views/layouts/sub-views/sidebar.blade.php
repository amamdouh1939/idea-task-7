<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="{{ route('home') }}" class="brand-link">
    <img src="{{ asset('backoffice/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
    <span class="brand-text font-weight-bold">CRM Application</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
    <!-- Leads List -->
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview menu-open">
        <a href="#" class="nav-link {{ Request::is('leads*') ? 'active' : '' }}">
            <i class="nav-icon fas fa-question"></i>
            <p>
            Leads
            <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if(Auth::user()->role->slug === "ADMIN")
                <li class="nav-item">
                <a href="{{ route('leads.create') }}" class="nav-link {{ Request::is('leads/create') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add New Lead</p>
                </a>
                </li>
            @endif
            <li class="nav-item">
            <a href="{{ route('leads.index') }}" class="nav-link {{ Request::is('leads') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>View All Leads</p>
            </a>
            </li>
        </ul>
        </li>
    </ul>
    <!-- End Leads List -->

    @if(Auth::user()->role->slug == 'ADMIN' || Auth::user()->role->slug == 'TMLDR')
    <!-- Users List -->
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview menu-open">
        <a href="#" class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
            <i class="nav-icon fas fa-user"></i>
            <p>
            Users
            <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if(Auth::user()->role->slug === "ADMIN")
                <li class="nav-item">
                <a href="{{ route('users.create') }}" class="nav-link {{ Request::is('users/create') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add New User</p>
                </a>
                </li>
            @endif
            <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link {{ Request::is('users') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>View All Users</p>
            </a>
            </li>
        </ul>
        </li>
    </ul>
    @endif
    <!-- End Users List -->

    <!-- Calls List -->
    @if(Auth::user()->role->slug == 'ADMIN' || Auth::user()->role->slug == 'TMLDR' || Auth::user()->role->slug == 'SALES')
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview menu-open">
        <a href="#" class="nav-link {{ Request::is('calls*') ? 'active' : '' }}">
            <i class="nav-icon fas fa-phone"></i>
            <p>
            Calls
            <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="{{ route('calls.index') }}" class="nav-link {{ Request::is('calls') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>View All Calls</p>
            </a>
            </li>
        </ul>
        </li>
    </ul>
    @endif
    <!-- End Calls List -->

    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
