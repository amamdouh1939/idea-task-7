<?php 
    $employees = array(
        [
            "name" => "Baby West",
            "company" => "Gutowski-Stokes",
            "email" => "britney96@example.org"
        ],
        [
            "name" => "Meghan Russel",
            "company" => "Crist-Nitzsche",
            "email" => "vkihn@example.net"
        ],
        [
            "name" => "Dr. Jannie Prosacco",
            "company" => "Bernhard-Beer",
            "email" => "dare.vivianne@example.org"
        ],
        [
            "name" => "Bud Hill",
            "company" => "Schneider, Maggio and Gibson",
            "email" => "natasha.prohaska@example.com"
        ],
        [
            "name" => "Kayleigh Weissnat",
            "company" => "Wolf-Cummings",
            "email" => "zechariah.schmitt@example.org"
        ],
        [
            "name" => "Margaret Volkman",
            "company" => "Trantow-Romaguera",
            "email" => "randall69@example.net" 
        ],
        [
            "name" => "Kaleigh Kshlerin",
            "company" => "Kihn, Swaniawski and Lebsack",
            "email" => "winfield24@example.net"
        ],
        [
            "name" => "Demo Admin",
            "company" => "Monahan PLC",
            "email" => "admin@admin.com"
        ],
        [
            "name" => "Demo Student",
            "company" => "Hickle, Rempel and Gerhold",
            "email" => "student@student.com"
        ]
    );
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Employee Data</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/base.css">
    </head>
    <body>
        <div class="container">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($employees as $employee) {
                    ?>
                        <tr>
                            <td><div class="avatar">
                                <img src="img/avatar-1295429_960_720.png" alt="">
                            </div></td>
                            <td><a href="#" class="name"><?php echo $employee["name"] ?></a></td>
                            <td><p><?php echo $employee["company"] ?></p></td>
                            <td><p class="email"><?php echo $employee["email"] ?></p></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>


        <script src="js/jquery-3.4.0.min.js"></script>    
        <script src="js/bootstrap.min.js"></script>    
    </body>
</html>