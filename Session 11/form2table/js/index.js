document.addEventListener("DOMContentLoaded", function(){
    let employee_form = document.querySelector("#employee-form");
    let save_button = document.querySelector("#save-button");

    save_button.addEventListener("click", function(){
        employee_form.submit();
    });
});