<?php 
    $employees = array(
        [
            "name" => "Baby West",
            "company" => "Gutowski-Stokes",
            "email" => "britney96@example.org"
        ],
        [
            "name" => "Meghan Russel",
            "company" => "Crist-Nitzsche",
            "email" => "vkihn@example.net"
        ],
        [
            "name" => "Dr. Jannie Prosacco",
            "company" => "Bernhard-Beer",
            "email" => "dare.vivianne@example.org"
        ],
        [
            "name" => "Bud Hill",
            "company" => "Schneider, Maggio and Gibson",
            "email" => "natasha.prohaska@example.com"
        ],
        [
            "name" => "Kayleigh Weissnat",
            "company" => "Wolf-Cummings",
            "email" => "zechariah.schmitt@example.org"
        ],
        [
            "name" => "Margaret Volkman",
            "company" => "Trantow-Romaguera",
            "email" => "randall69@example.net" 
        ],
        [
            "name" => "Kaleigh Kshlerin",
            "company" => "Kihn, Swaniawski and Lebsack",
            "email" => "winfield24@example.net"
        ],
        [
            "name" => "Demo Admin",
            "company" => "Monahan PLC",
            "email" => "admin@admin.com"
        ],
        [
            "name" => "Demo Student",
            "company" => "Hickle, Rempel and Gerhold",
            "email" => "student@student.com"
        ]
    );

    if (isset($_POST["name"]) && isset($_POST["company"]) && isset($_POST["email"])){
        $employee_name = $_POST["name"];
        $employee_company = $_POST["company"];
        $employee_email = $_POST["email"];
    
        $employees[] = [
            "name" => $employee_name,
            "company" => $employee_company,
            "email" => $employee_email
        ];
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Employee Data</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/base.css">
    </head>
    <body>
        <div class="container">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($employees as $employee) {
                    ?>
                        <tr>
                            <td><div class="avatar">
                                <img src="img/avatar-1295429_960_720.png" alt="">
                            </div></td>
                            <td><a href="#" class="name"><?php echo $employee["name"] ?></a></td>
                            <td><p><?php echo $employee["company"] ?></p></td>
                            <td><p class="email"><?php echo $employee["email"] ?></p></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <hr>
            <form id="employee-form" action="" method="post">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Personal Details</h2>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <a id="save-button" class="btn btn-success mt-5"><i class="fas fa-save"></i> Save</a>
                        <a href="" class="btn btn-default"><i class="far fa-times-circle"></i> Cancel</a>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" placeholder="Enter User's Name">
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <input name="company" type="text" class="form-control" placeholder="Enter User's Company">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control" placeholder="Enter User's Email">
                </div>
            </form>
        </div>

        <script src="js/jquery-3.4.0.min.js"></script>    
        <script src="js/bootstrap.min.js"></script>    
        <script src="js/index.js"></script>
    </body>
</html>