<!DOCTYPE html>
<html>
    <head>
        <title>Employee Data</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="css/base.css">
    </head>
    <body>
        <div class="container">
            <form action="#" method="post">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Personal Details</h2>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <a href="" class="btn btn-success mt-5"><i class="fas fa-save"></i> Save</a>
                        <a href="" class="btn btn-default"><i class="far fa-times-circle"></i> Cancel</a>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" placeholder="Enter User's Name">
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <input name="company" type="text" class="form-control" placeholder="Enter User's Company">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control" placeholder="Enter User's Email">
                </div>
            </form>
        </div>


        <script src="js/jquery-3.4.0.min.js"></script>    
        <script src="js/bootstrap.min.js"></script>    
    </body>
</html>