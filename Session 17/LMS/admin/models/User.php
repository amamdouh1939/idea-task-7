<?php

    require_once 'config/dbConnection.php';

    class User{
        private static $dbh;
        private $id;
        private $role;
        private $firstName;
        private $lastName;
        private $username;
        private $email;
        private $phone;
        private $password;

        private static function setDbh(){
            global $dbh;
            self::$dbh = $dbh;
        }

        public function __construct()
        {
            global $dbh;
            self::$dbh = $dbh;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function getRole(){
            return $this->role;
        }

        public function setRole($role){
            $this->role = $role;
        }

        public function getFirstName(){
            return $this->firstName;
        }

        public function setFirstName($firstName){
            $this->firstName = $firstName;
        }

        public function getLastName(){
            return $this->lastName;
        }

        public function setLastName($lastName){
            $this->lastName = $lastName;
        }

        public function getUsername(){
            return $this->username;
        }

        public function setUsername($username){
            $this->username = $username;
        }

        public function getEmail(){
            return $this->email;
        }

        public function setEmail($email){
            $this->email = $email;
        }

        public function getPhone(){
            return $this->phone;
        }

        public function setPhone($phone){
            $this->phone = $phone;
        }

        public function getPassword(){
            return $this->password;
        }

        public function setPassword($password){
            $this->password = $password;
        }

        public static function all(){
            self::setDbh();
            $sql = 'SELECT * FROM users';
            $smt = self::$dbh->prepare($sql);
            $smt->execute();

            return $smt->fetchAll(PDO::FETCH_OBJ);
        }

        public static function find($id){
            self::setDbh();
            $sql = 'SELECT * FROM users WHERE user_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "id" => $id
            ];

            $smt->execute($data);

            return $smt->fetch(PDO::FETCH_OBJ);
        }

        public function insert(){
            $sql = 'INSERT INTO users (role, first_name, last_name, username, email, phone, password) VALUES (:role, :firstName, :lastName, :username, :email, :phone, :password)';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "role" => $this->role,
                "firstName" => $this->firstName,
                "lastName" => $this->lastName,
                "username" => $this->username,
                "email" => $this->email,
                "phone" => $this->phone,
                "password" => $this->password
            ];

            $smt->execute($data);

            return self::$dbh->lastInsertId();
        }

        public static function delete($id){
            self::setDbh();
            $sql = 'DELETE FROM users WHERE user_id=:id';
            $smt = self::$dbh->prepare($sql);
            
            $data = [
                "id" => $id
            ];

            $smt->execute($data);
        }

        public function update(){
            $sql = 'UPDATE users SET role=:role, first_name=:firstName, last_name=:lastName, username=:username, email=:email, phone=:phone, password=:password WHERE user_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "role" => $this->role,
                "firstName" => $this->firstName,
                "lastName" => $this->lastName,
                "username" => $this->username,
                "email" => $this->email,
                "phone" => $this->phone,
                "password" => $this->password,
                "id" => $this->id
            ];

            $smt->execute($data);
        }

    }