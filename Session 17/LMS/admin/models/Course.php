<?php

    require_once 'config/dbConnection.php';

    class Course{
        private static $dbh;
        private $id;
        private $name;
        private $duration;
        private $level;
        private $description;
        private $teacherId;

        public function __construct()
        {
            global $dbh;
            self::$dbh = $dbh;
        }

        private static function setDbh(){
            global $dbh;
            self::$dbh = $dbh;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function getName(){
            return $this->name;
        }

        public function setName($name){
            $this->name = $name;
        }

        public function getDuration(){
            return $this->duration;
        }

        public function setDuration($duration){
            $this->duration = $duration;
        }

        public function getLevel(){
            return $this->level;
        }

        public function setLevel($level){
            $this->level = $level;
        }

        public function getDescription(){
            return $this->description;
        }

        public function setDescription($description){
            $this->description = $description;
        }

        public function getTeacherId(){
            return $this->teacherId;
        }

        public function setTeacherId($teacherId){
            $this->teacherId = $teacherId;
        }

        public static function all(){
            self::setDbh();
            $sql = 'SELECT course_id, name, duration, level, courses.description AS course_description, teachers.description, username, first_name, last_name, username, email, phone FROM courses INNER JOIN teachers ON courses.teacher_id=teachers.user_id INNER JOIN users ON teachers.user_id=users.user_id';
            $smt = self::$dbh->prepare($sql);

            $smt->execute();

            return $smt->fetchAll(PDO::FETCH_OBJ);
        }

        public static function find($id){
            self::setDbh();
            $sql = 'SELECT * FROM courses WHERE course_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = ["id" => $id];

            $smt->execute($data);

            return $smt->fetch(PDO::FETCH_OBJ);
        }

        public function insert(){
            $sql = 'INSERT INTO courses (name, duration, level, description, teacher_id) VALUES (:name, :duration, :level, :description, :teacher_id)';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "name" => $this->name,
                "duration" => $this->duration,
                "level" => $this->level,
                "description" => $this->description,
                "teacher_id" => $this->teacherId
            ];

            $smt->execute($data);

            return self::$dbh->lastInsertId();
        }

        public function update(){
            $sql = 'UPDATE courses SET name=:name, duration=:duration, level=:level, description=:description, teacher_id=:teacher_id WHERE course_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "id" => $this->id,
                "name" => $this->name,
                "duration" => $this->duration,
                "level" => $this->level,
                "description" => $this->description,
                "teacher_id" => $this->teacherId
            ];

            $smt->execute($data);

        }

        public static function delete($id){
            self::setDbh();
            $sql = 'DELETE FROM courses WHERE course_id=:id';
            $smt = self::$dbh->prepare($sql);
            $data = ["id" => $id];

            $smt->execute($data);
        }
    };