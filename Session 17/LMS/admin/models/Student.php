<?php
    require_once 'config/dbConnection.php';
    
    class Student{
        private static $dbh;
        private $id;
        private $description;

        private static function setDbh(){
            global $dbh;
            self::$dbh = $dbh;
        }

        public function __construct()
        {
            global $dbh;
            self::$dbh = $dbh;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function getId(){
            return $this->id;
        }

        public function getDescription(){
            return $this->description;
        }

        public function setDescription($description){
            $this->description = $description;
        }

        public static function allStudents(){
            self::setDbh();

            $sql =  'SELECT * FROM users WHERE role="STND"';

            $smt = self::$dbh->prepare($sql);

            $smt->execute();

            return $smt->fetchAll(PDO::FETCH_OBJ);

        }

        public static function checkUsersForStudents(){
            self::setDbh();
            $sql = 'SELECT * FROM users WHERE role="STND"';

            $smt = self::$dbh->prepare($sql);

            $smt->execute();

            $dataFromUsersTable = $smt->fetchAll(PDO::FETCH_OBJ);

            foreach ($dataFromUsersTable as $element) {
                $newStudent = new Student();
                $newStudent->setId($element->user_id);
                $newStudent->insertId();
            }
        }

        public static function checkNotStudentUsers(){
            self::setDbh();
            $sql = 'SELECT * FROM users WHERE role = "TCHR"';

            $smt = self::$dbh->prepare($sql);

            $smt->execute();

            $dataFromUsersTable = $smt->fetchAll(PDO::FETCH_OBJ);

            foreach ($dataFromUsersTable as $element) {
                if ($element->role == "TCHR"){
                    Student::delete($element->user_id);
                }
            }
        }

        public static function all(){
            self::setDbh();
            $sql = 'SELECT students.user_id, username, first_name, last_name, email, phone, description FROM students INNER JOIN users ON users.user_id = students.user_id';
            $smt = self::$dbh->prepare($sql);
            $smt->execute();

            return $smt->fetchAll(PDO::FETCH_OBJ);
        }

        private function insertId(){
            $sql = 'INSERT INTO students (user_id) VALUES (:user_id)';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "user_id" => $this->id
            ];

            $smt->execute($data);
        }

        public function insert(){
            $sql = 'INSERT INTO students (description) VALUES (:description) WHERE user_id=:user_id';
            $smt = self::$dbh->prepare($sql);
            self::setId($this->id);

            $data = [
                "description" => $this->description,
                "user_id" => $this->id
            ];

            $smt->execute($data);
        }

        public static function find($id){
            self::setDbh();
            $sql = 'SELECT students.user_id, username, first_name, last_name, email, phone, description FROM students JOIN users ON users.user_id = students.user_id WHERE students.user_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = ["id" => $id];
            $smt->execute($data);

            return $smt->fetch(PDO::FETCH_OBJ);
        }

        public function update(){
            $sql = 'UPDATE students SET description=:description WHERE user_id=:id';
            $smt = self::$dbh->prepare($sql);

            $data = [
                "description" => $this->description,
                "id" => $this->id
            ];

            $smt->execute($data);
        }

        public static function delete($id){
            self::setDbh();
            $sql = 'DELETE FROM students WHERE user_id=:id';

            $smt = self::$dbh->prepare($sql);
            $data = ["id" => $id];
            $smt->execute($data);
        }
    };