<?php

    $page = $_GET['page'];

    if (!isset($_GET['page']) || empty($_GET['page'])){
        require_once 'views/index.php';
    } else {

        switch ($page) {
            case 'users':
                require_once 'views/users.php';
                break;
    
            case 'teachers':
                require_once 'views/teachers.php';
                break;
    
            case 'students':
                require_once 'views/students.php';
                break;
            
            case 'courses':
                require_once 'views/courses.php';
                break;

            case 'user_show':
                require_once 'views/user-show.php';
                break;
            
            case 'user_edit':
                require_once 'views/user-edit.php';
                break;

            case 'teacher_show':
                require_once 'views/teacher-show.php';
                break;
            
            case 'teacher_edit':
                require_once 'views/teacher-edit.php';
                break;

            case 'student_show':
                require_once 'views/student-show.php';
                break;

            case 'student_edit':
                require_once 'views/student-edit.php';
                break;

            case 'course_show':
                require_once 'views/course-show.php';
                break;
            
            case 'course_edit':
                require_once 'views/course-edit.php';
    
            default:
                require_once 'views/404.php';
                break;
            
        }
    }