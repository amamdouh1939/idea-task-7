$(document).ready( function () {
    $('#teachersTable').DataTable();
});

$(document).ready( function () {
    $('#studentsTable').DataTable();
});

$(document).ready( function () {
    $('#coursesTable').DataTable();
    $('.select-menu').select2({width: "100%",
                               dropdownParent: $('#add-new')});
});

$(document).ready( function () {
    $('#usersTable').DataTable();
} );

CKEDITOR.replace('desc');

function swalPopup(){
    Swal.fire({
    title: 'Warning!',
    text: 'Are you sure you want to delete this?',
    type: 'warning',
    confirmButtonText: 'Yes',
    showCancelButton: true,
    cancelButtonText: 'No'
    });
}