# Summary

Date : 2019-06-27 22:56:16

Directory /home/amamdouh1939/Desktop/IDEA-TASK-7/Session 17/LMS/admin

Total : 1641 files,  96029 codes, 15289 comments, 17733 blanks, all 129051 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 36 | 51,621 | 15,070 | 11,437 | 78,128 |
| CSS | 20 | 18,498 | 84 | 4,880 | 23,462 |
| XML | 1,522 | 14,850 | 24 | 6 | 14,880 |
| SCSS | 18 | 4,562 | 56 | 523 | 5,141 |
| Less | 18 | 4,543 | 55 | 520 | 5,118 |
| PHP | 25 | 1,859 | 0 | 365 | 2,224 |
| JSON | 1 | 81 | 0 | 1 | 82 |
| Log | 1 | 15 | 0 | 1 | 16 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 1,641 | 96,029 | 15,289 | 17,733 | 129,051 |
| config | 2 | 28 | 0 | 4 | 32 |
| libs | 4 | 75 | 0 | 48 | 123 |
| models | 3 | 342 | 0 | 115 | 457 |
| public | 1,615 | 94,155 | 15,289 | 17,367 | 126,811 |
| public/css | 3 | 10,611 | 19 | 1,393 | 12,023 |
| public/js | 8 | 692 | 136 | 94 | 922 |
| public/js/demo | 4 | 248 | 15 | 9 | 272 |
| public/vendor | 1,604 | 82,852 | 15,134 | 15,880 | 113,866 |
| public/vendor/bootstrap | 2 | 2 | 11 | 0 | 13 |
| public/vendor/bootstrap/css | 1 | 1 | 5 | 0 | 6 |
| public/vendor/bootstrap/js | 1 | 1 | 6 | 0 | 7 |
| public/vendor/chart.js | 4 | 24,333 | 4,606 | 5,047 | 33,986 |
| public/vendor/datatables | 6 | 6,828 | 7,019 | 2,020 | 15,867 |
| public/vendor/fontawesome-free | 1,585 | 39,491 | 357 | 5,283 | 45,131 |
| public/vendor/fontawesome-free/css | 14 | 7,688 | 60 | 3,476 | 11,224 |
| public/vendor/fontawesome-free/js | 12 | 7,767 | 162 | 757 | 8,686 |
| public/vendor/fontawesome-free/less | 18 | 4,543 | 55 | 520 | 5,118 |
| public/vendor/fontawesome-free/scss | 18 | 4,562 | 56 | 523 | 5,141 |
| public/vendor/fontawesome-free/sprites | 3 | 4,557 | 12 | 3 | 4,572 |
| public/vendor/fontawesome-free/svgs | 1,516 | 1,516 | 0 | 0 | 1,516 |
| public/vendor/fontawesome-free/svgs/brands | 427 | 427 | 0 | 0 | 427 |
| public/vendor/fontawesome-free/svgs/regular | 152 | 152 | 0 | 0 | 152 |
| public/vendor/fontawesome-free/svgs/solid | 937 | 937 | 0 | 0 | 937 |
| public/vendor/fontawesome-free/webfonts | 3 | 8,777 | 12 | 3 | 8,792 |
| public/vendor/jquery | 4 | 11,997 | 3,123 | 3,521 | 18,641 |
| public/vendor/jquery-easing | 3 | 201 | 18 | 9 | 228 |
| views | 16 | 1,388 | 0 | 185 | 1,573 |
| views/tpl | 2 | 303 | 0 | 54 | 357 |

[details](details.md)