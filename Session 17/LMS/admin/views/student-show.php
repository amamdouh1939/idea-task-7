<?php
    require_once 'tpl/header.php';
    require_once 'models/Student.php';

    if (isset($_GET["id"])){
        $student = Student::find($_GET["id"]);
    }

?>
    <div class="info-area">
        <h1>Information about <?php echo $student->username ?></h1>
        <hr>
        <h3>Username: <?php echo $student->username; ?></h3>
        <h3>Full Name: <?php echo $student->first_name . ' ' . $student->last_name ?></h3>
        <h3>Email: <?php echo $student->email ?></h3>
        <h3>Phone Number: <?php echo $student->phone ?></h3> 
        <hr>
        <h4>Description</h4>
        <p><?php echo $student->description ?></p> 
    </div>
<?php
    require_once 'tpl/footer.php';
?>