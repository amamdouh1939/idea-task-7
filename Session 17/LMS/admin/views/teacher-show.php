<?php
    require_once 'tpl/header.php';
    require_once 'models/Teacher.php';

    if (isset($_GET["id"])){
        $teacher = Teacher::find($_GET["id"]);
    }

?>
    <div class="info-area">
        <h1>Information about <?php echo $teacher->username ?></h1>
        <hr>
        <h3>Username: <?php echo $teacher->username; ?></h3>
        <h3>Full Name: <?php echo $teacher->first_name . ' ' . $teacher->last_name ?></h3>
        <h3>Email: <?php echo $teacher->email ?></h3>
        <h3>Phone Number: <?php echo $teacher->phone ?></h3> 
        <hr>
        <h4>Description</h4>
        <p><?php echo $teacher->description ?></p> 
    </div>
<?php
    require_once 'tpl/footer.php';
?>