<?php
    require_once 'tpl/header.php';
    require_once 'models/User.php';

    if (isset($_GET["id"])){
        $user = User::find($_GET["id"]);
        if ($user->role == "TCHR"){
            $role = "Teacher";
        } elseif ($user->role == "STND"){
            $role = "Student";
        }
    }
?>
    <div class="info-area">
        <h1>Information about <?php echo $user->username ?></h1>
        <hr>
        <h3>Username: <?php echo $user->username; ?></h3>
        <h3>Role: <?php echo $role ?></h3>
        <h3>Full Name: <?php echo $user->first_name . ' ' . $user->last_name ?></h3>
        <h3>Email: <?php echo $user->email ?></h3>
        <h3>Phone Number: <?php echo $user->phone ?></h3> 
    </div>
<?php
    require_once 'tpl/footer.php';
?>