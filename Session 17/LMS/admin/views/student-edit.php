<?php
    require_once 'tpl/header.php';
    require_once 'models/User.php';
    require_once 'models/Student.php';

    if (isset($_GET["id"]) && !isset($_GET["blank"])){
        $student = Student::find($_GET["id"]);
    }

    if (isset($_POST["id"], $_POST["first_name"], $_POST["last_name"], $_POST["username"], $_POST["email"], $_POST["phone"])){
        $updatedUser = new User();
        $updatedUser->setId($_POST["id"]);
        $updatedUser->setFirstName($_POST["first_name"]);
        $updatedUser->setLastName($_POST["last_name"]);
        $updatedUser->setUsername($_POST["username"]);
        $updatedUser->setEmail($_POST["email"]);
        $updatedUser->setPhone($_POST["phone"]);
        $updatedUser->setPassword($_POST["password"]);
        $updatedUser->setRole($_POST["role"]);

        $updatedUser->update();

        $updatedStudent = new Student();
        $updatedStudent->setId($_POST["id"]);
        $updatedStudent->setDescription($_POST["description"]);

        $updatedStudent->update();

    }
?>
    <div class="info-area">
        <h1>Edit Student <?php echo $student->username ?></h1>
        <hr>
        <form action="?page=student_edit&id=<?php echo $student->user_id ?>&blank=true" method="POST">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id" value="<?php echo $student->user_id ?>">
                    </div>
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" name="first_name" class="form-control" value="<?php echo $student->first_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" value="<?php echo $student->last_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $student->username ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $student->email ?>">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="tel" name="phone" class="form-control" value="<?php echo $student->phone ?>">
                    </div>
                    <div class="form-group">
                        <label for="desc">Description</label>
                                <textarea id="desc"
                                        class="form-control"
                                        cols="30"
                                        rows="10" name="description"><?php echo $student->description ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="password" value="<?php echo $student->password ?>">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="role" value="<?php echo $student->role ?>">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
<?php
    require_once 'tpl/footer.php';
?>