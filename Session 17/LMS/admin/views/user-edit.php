<?php
    require_once 'tpl/header.php';
    require_once 'models/User.php';

    if (isset($_GET["id"]) & !isset($_GET["blank"])){
        $user = User::find($_GET["id"]);
    }

    if (isset($_POST["id"], $_POST["role"], $_POST["first_name"], $_POST["last_name"], $_POST["username"], $_POST["email"], $_POST["phone"], $_POST["password"])){
        $updatedUser = new User();
        $updatedUser->setId($_POST["id"]);
        $updatedUser->setRole($_POST["role"]);
        $updatedUser->setFirstName($_POST["first_name"]);
        $updatedUser->setLastName($_POST["last_name"]);
        $updatedUser->setUsername($_POST["username"]);
        $updatedUser->setEmail($_POST["email"]);
        $updatedUser->setPhone($_POST["phone"]);
        $updatedUser->setPassword($_POST["password"]);

        $updatedUser->update();

    }
?>
    <div class="info-area">
        <h1>Edit user <?php echo $user->username ?></h1>
        <hr>
        <form action="?page=user_edit&id=<?php echo $user->user_id ?>&blank=true" method="POST">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id" value="<?php echo $user->user_id ?>">
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select class="form-control" name="role" id="role">
                            <option value="TCHR">Teacher</option>
                            <option value="STND">Student</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" name="first_name" class="form-control" value="<?php echo $user->first_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" value="<?php echo $user->last_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $user->username ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $user->email ?>">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="tel" name="phone" class="form-control" value="<?php echo $user->phone ?>">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" value="<?php echo $user->password ?>">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
<?php
    require_once 'tpl/footer.php';
?>