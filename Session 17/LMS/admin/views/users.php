<?php require_once 'tpl/header.php';
      require_once 'config/dbConnection.php';
      require_once 'models/User.php';

      if (isset($_GET["action"]) && !empty($_GET["action"])){
          switch ($_GET["action"]){
              case "add_new":
                $newUser = new User();
                $newUser->setRole($_POST["role"]);
                $newUser->setFirstName($_POST["first_name"]);
                $newUser->setLastName($_POST["last_name"]);
                $newUser->setUsername($_POST["username"]);
                $newUser->setEmail($_POST["email"]);
                $newUser->setPhone($_POST["phone"]);
                $newUser->setPassword($_POST["password"]);

                $newUser->insert();
                break;

            case "delete":
                User::delete($_GET["id"]);
                break;

            default:
                break;
          }
      }

      $users = User::all();

?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Users</h1>

                <div class="card">
                    <button type="button"
                            class="btn btn-success m-3"
                            data-toggle="modal"
                            data-target="#add-new">
                        Add New
                    </button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="usersTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($users as $key => $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item->user_id ?></td>
                                        <td><?php echo $item->first_name . ' ' . $item->last_name ?></td>
                                        <td><?php echo $item->email ?></td>
                                        <td>
                                            <a href="?page=user_show&id=<?php echo $item->user_id ?>" class="btn btn-info">Show</a>
                                            <a href="?page=user_edit&id=<?php echo $item->user_id ?>" class="btn btn-primary">Edit</a>
                                            <a href="?page=users&action=delete&id=<?php echo $item->user_id ?>" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- Modal -->

<!-- Add New Modal -->
<div class="modal fade" id="add-new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="?page=users&action=add_new" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="role">
                                    Role
                                </label>
                                <select name="role" id="role">
                                    <option value="TCHR">Teacher</option>
                                    <option value="STND">Student</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="first-name">First Name</label>
                                <input id="first-name" type="text" name="first_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="last-name">Last Name</label>
                                <input id="last-name" type="text" name="last_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" type="text" name="username" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" name="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input id="phone" type="tel" name="phone" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input id="password" type="password" name="password" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>
        <!-- End of Main Content -->

    <?php require_once 'tpl/footer.php' ?>