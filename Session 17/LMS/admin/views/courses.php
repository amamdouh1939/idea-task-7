<?php require_once 'tpl/header.php';
      require_once 'models/Teacher.php';
      require_once 'models/Course.php';

      $teachers = Teacher::all();
      $courses = Course::all();

      if (isset($_GET["action"]) && $_GET["action"] == "add_new"){
          if (isset($_POST["name"], $_POST["duration"], $_POST["level"], $_POST["description"], $_POST["teacher_id"])){
              $newCourse = new Course();
              $newCourse->setName($_POST["name"]);
              $newCourse->setDuration($_POST["duration"]);
              $newCourse->setLevel($_POST["level"]);
              $newCourse->setDescription($_POST["description"]);
              $newCourse->setTeacherId($_POST["teacher_id"]);

              $newCourse->insert();
          }
      }

      if (isset($_GET["action"]) && $_GET["action"] == "delete"){
          Course::delete($_GET["id"]);
      }

?>

<!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Courses</h1>

                <div class="card">
                    <!-- Button trigger modal -->
                    <button type="button"
                            class="btn btn-success m-3"
                            data-toggle="modal"
                            data-target="#add-new" id="add-new-button">
                        Add New
                    </button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="coursesTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Duration</th>
                                <th>Level</th>
                                <th>Pre-Quizzed</th>
                                <th>Content</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($courses as $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item->course_id; ?></td>
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->duration; ?>h</td>
                                        <td><?php echo $item->level; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-primary bg-secondary">Courses</a>
                                        </td>
                                        <td>
                                            <a href="course-content.html" class="btn btn-success">Content</a>
                                        </td>
                                        <td>
                                            <a href="?page=course_show&id=<?php echo $item->course_id ?>" class="btn btn-info">Show</a>
                                            <a href="?page=course_edit&id=<?php echo $item->course_id ?>" class="btn btn-primary">Edit</a>
                                            <a href="?page=courses&action=delete&id=<?php echo $item->course_id ?>" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Modal -->

<div class="modal fade" id="add-new" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="?page=courses&action=add_new" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">
                                    Name:
                                </label>
                                <input id="name" name="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="duration">Duration</label>
                                <input id="duration"
                                       name="duration"
                                       type="number"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select id="level" name="level" class="form-control select-menu" name="level">
                                    <option value="Level 1">Level 1</option>
                                    <option value="Level 2">Level 2</option>
                                    <option value="Level 3">Level 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="courses">Teacher</label>
                                <select id="courses" name="teacher_id" class="form-control select-menu">
                                    <?php
                                        foreach ($teachers as $teacher){
                                    ?>
                                        <option value="<?php echo $teacher->user_id ?>"><?php echo $teacher->first_name . ' ' . $teacher->last_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="desc">Description</label>
                            <textarea id="desc"
                                      name="description"
                                      class="form-control"
                                      cols="30"
                                      rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>

    <?php require_once 'tpl/footer.php' ?>