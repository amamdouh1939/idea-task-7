<?php

    $dbhost = 'localhost';
    $dbname = 'LMS_TASK';
    $dbpassword = '2871991i';
    $dbuser = 'pmauser';

    $dsn = "mysql:host=$dbhost;dbname=$dbname";

    try {
        $dbh = new PDO($dsn, $dbuser, $dbpassword);
    } catch (Exception $e) {
        $currentTime = date('D, d M y H:i:s');
        $errorMessage = $currentTime . ' => ' . $e->getMessage() . "\n";
        file_put_contents('config/dbError.log', $errorMessage, FILE_APPEND);
    }