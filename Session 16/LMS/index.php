<?php

    require_once 'config/dbConnection.php';
    require_once 'models/User.php';

    $user = new User();
    $user->setUsername('magdy');
    $user->setName('Magdy');
    $user->setEmail('magdy@magdy.com');
    $user->setPhone('01010105123');

    echo $user->insert();