<?php

    class User{
        private static $dbh;
        private $id;
        private $username;
        private $name;
        private $email;
        private $phone;


        public function __construct(){
            global $dbh;
            self::$dbh = $dbh;
        }

        private static function setDbh(){
            global $dbh;
            return $dbh;
        }

        public function getId():int
        {
            return $this->id;
        }

        public function getUsername():string
        {
            return $this->username;
        }

        public function setUsername(string $username){
            $this->username = $username;
        }

        public function getName():string
        {
            return $this->name;
        }

        public function setName(string $name){
            $this->name = $name;
        }

        public function getEmail():string
        {
            return $this->email;
        }

        public function setEmail(string $email){
            $this->email = $email;
        }

        public function getPhone():string
        {
            return $this->phone;
        }

        public function setPhone(string $phone){
            $this->phone = $phone;
        }

        public static function all(){
            $dbh = self::setDbh();
            $sql = 'SELECT * FROM Users';
            $smt = $dbh->prepare($sql);
            $smt->execute();

            $data = $smt->fetchAll(PDO::FETCH_CLASS);

            return $data;
        }

        public static function find(int $id){
            $dbh = self::setDbh();
            $sql = 'SELECT * FROM Users WHERE user_id = :id';
            $smt = $dbh->prepare($sql);

            $data = [
                'id' => $id
            ];

            $smt->execute($data);
            $userData = $smt->fetchAll(PDO::FETCH_CLASS);
            return $userData;
        }

        public function insert(){
            $sql = 'INSERT INTO Users (username, name, email, phone) VALUES (:username, :name, :email, :phone)';
            $smt = self::$dbh->prepare($sql);

            $data = [
                'username' => $this->username,
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone
            ];

            $smt->execute($data);

            return self::$dbh->lastInsertId();
        }
    };