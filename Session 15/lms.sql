CREATE DATABASE LMS;

USE LMS;

CREATE TABLE Users (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(60) UNIQUE NOT NULL,
    name VARCHAR(80),
    email VARCHAR(60) UNIQUE,
    phone NCHAR(11) UNIQUE NOT NULL
);

CREATE TABLE Teachers (
    teacher_id INT PRIMARY KEY AUTO_INCREMENT,
    description TEXT,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) 
);

CREATE TABLE Students (
    student_id INT PRIMARY KEY AUTO_INCREMENT,
    description TEXT,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

CREATE TABLE Courses (
    course_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) UNIQUE NOT NULL,
    duration INT,
    level VARCHAR(60)
);

CREATE TABLE Rounds (
    round_id INT PRIMARY KEY AUTO_INCREMENT,
    course_id INT NOT NULL,
    teacher_id INT NOT NULL,
    FOREIGN KEY (course_id) REFERENCES Courses(course_id),
    FOREIGN KEY (teacher_id) REFERENCES Teachers(teacher_id)
);

CREATE TABLE Rounds_Students (
    round_id INT NOT NULL,
    student_id INT NOT NULL,
    FOREIGN KEY (round_id) REFERENCES Rounds(round_id),
    FOREIGN KEY (student_id) REFERENCES Students(student_id),
    PRIMARY KEY (round_id, student_id)
);