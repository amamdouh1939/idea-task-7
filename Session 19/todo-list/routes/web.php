<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'TodoController@index')->name('todos.index');
Route::post('/', 'TodoController@insert')->name('todos.insert');
Route::get('/edit/{id}', 'TodoController@editView')->where('id', '[0-9]+')->name('todos.edit_view');
Route::post('/edit', 'TodoController@edit')->name('todos.edit');
Route::get('/delete/{id}', 'TodoController@delete')->where('id', '[0-9]+')->name('todos.delete');
