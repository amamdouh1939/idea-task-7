@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="text-center">Todo List</h1>
    <ul>
        @foreach($todos as $todo)
            <div class="todo-item" style="margin: 10px;">
                <li style="display: inline-block; width: 75%;">{{ $todo->todo_text }}</li>
                <a style="display: inline-block; width: 10%;" href="{{ route('todos.edit_view', $todo->id )}}" class="btn btn-warning">Edit</a>
                <a style="display: inline-block; width: 10%;" href="{{ route('todos.delete', $todo->id )}}" class="btn btn-danger">Delete</a>
            </div>
        @endforeach
    </ul>
    <hr>
    <form action="{{ route('todos.insert') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="todo">Add new todo</label>
            <input type="text" name="todo" id="todo" class="form-control">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>
</div>
@endsection