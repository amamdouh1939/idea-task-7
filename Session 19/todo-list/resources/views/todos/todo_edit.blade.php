@extends('layouts.app')

@section('content')

<div class="container text-center">
    <form action="{{ route('todos.edit') }}" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{ $todo->id }}">
        <div class="form-group">
            <label for="todo">Edit Todo</label>
            <input class="form-control" type="text" name="todo" id="todo" value="{{ $todo->todo_text }}">
        </div>
        <div class="form-group text-center">
            <button class="btn btn-primary" type="submit">Save</button>
        </div>
    </form>
</div>

@endsection