<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Auth;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $userId = \Auth::user()->id;
        $todos = Todo::where('user_id', $userId)->get();

        return view('todos.todos', ["todos" => $todos]);
    }

    public function insert(Request $request){
        $todo = new Todo();
        $todo->todo_text = $request['todo'];
        $todo->user_id = \Auth::user()->id;

        $todo->save();
        return redirect(route('todos.index'));
    }

    public function editView($id){
        return view('todos.todo_edit', ["todo" => Todo::find($id)]);
    }

    public function edit(Request $request){
        $todo = Todo::find($request['id']);
        $todo->todo_text = $request['todo'];
        $todo->save();

        return redirect(route('todos.index'));
    }

    public function delete($id){
        $todo = Todo::find($id);
        $todo->delete();

        return redirect(route('todos.index'));
    }
}