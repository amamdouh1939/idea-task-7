<!DOCTYPE html>
<html>
    <head>
        <title>Session 10 | Registeration</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-center">Welcome, <?php echo $_GET['name'] ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Name: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['name']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Username: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['username']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Email: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['email']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Password: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['password']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Re-Password: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['re_password']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Gender: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['gender']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>User Choices: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php
                            $selected_choices = $_GET['choices'];
                            for ($i = 0; $i < count($selected_choices); $i++){
                                if ($i < count($selected_choices) -1){
                                    echo $selected_choices[$i] . ", ";
                                } else {
                                    echo $selected_choices[$i];
                                }
                            }
                        ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Membership: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['membership']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Range: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['range']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Color: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['color']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Time: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['datetime']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Search: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['search']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>URL: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['url']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Number: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['number']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Phone: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['phone']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>Data List: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['datalist']; ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <p>User Bio: </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <?php echo $_GET['bio']; ?>
                    </p>
                </div>
            </div>


        </div>


        <script src="js/jquery-3.4.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>