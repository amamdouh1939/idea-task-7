<!DOCTYPE html>
<html>
    <head>
        <title>Session 10 | Homepage</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <style>

            .container {
                padding: 20px;
            }

            h1 {
                margin-bottom: 30px;    
            }

            .col-md-3, .col-md-9 {
                margin-bottom: 15px;
            }

            .col-md-3 label {
                font-size: 16px;
            }
        </style>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-center">Registeration Form</h1>
                    <form action="register.php" class="form" method="get">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="name">Name: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" id="name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="username">Username: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" id="username" name="username">
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="email">Email: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="email" id="email" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="password">Password: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="password" id="password" name="password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="re_password">Re-Password: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="password" id="re-password" name="re_password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="gender">Gender: </label>
                                </div>
                                <div class="col-md-9">
                                    Male: <input type="radio" name="gender" value="MALE">
                                    Female: <input type="radio" name="gender" value="FEMALE">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="choices[]">Choices: </label>
                                </div>
                                <div class="col-md-9">
                                    <input type="checkbox" name="choices[]" value="Choice 1"> Choice 1
                                    <input type="checkbox" name="choices[]" value="Choice 2"> Choice 2
                                    <input type="checkbox" name="choices[]" value="Choice 3"> Choice 3
                                    <input type="checkbox" name="choices[]" value="Choice 4"> Choice 4
                                    <input type="checkbox" name="choices[]" value="Choice 5"> Choice 5
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="membership">Membership: </label>
                                </div>
                                <div class="col-md-9">
                                    <select class="form-control" name="membership" id="membership">
                                        <option value="bronze">Bronze</option>
                                        <option value="silver">Silver</option>
                                        <option value="gold">Gold</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="range">Range: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="range" id="range" name="range" min="0" max="100" value="20" step="5">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="color">Color: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="color" name="color" id="color">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="datetime">Time: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="datetime-local" name="datetime" id="datetime">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="search">Search: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="search" name="search" id="search">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="url">URL: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="url" name="url" id="url">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="number">Number: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="number" name="number" id="number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="phone">Phone: </label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="tel" name="phone" id="phone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="datalist">Data List:</label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" list="datalist" name="datalist">
                                    <datalist id="datalist">
                                        <option value="Item 1">Item 1</option>
                                        <option value="Item 2">Item 2</option>
                                        <option value="Item 3">Item 3</option>
                                        <option value="Item 4">Item 4</option>
                                    </datalist>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="bio">About Me: </label>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="bio" id="bio" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
 

                        <div class="text-center">
                            <button class="btn btn-primary">Save</button>
                            <input type="reset" class="btn btn-danger">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        
        <script src="js/jquery-3.4.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>