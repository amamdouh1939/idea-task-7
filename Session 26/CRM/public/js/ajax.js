// AJAX for adding new user modal

$(document).on('click', '#addNewUserButton', function () {
    $.ajax({
        type: 'GET',
        url: '/users/create-form',
        success: function (res) {
            $('#addNewUserModal .modal-body').html(res);
        }
    });
});

// AJAX for editing user modal

$(document).on('click', '.editUserButton', function () {
    const userId = $(this).attr('user-id');

    $.ajax({
        type: 'GET',
        url: '/users/' + userId + '/edit-form',
        success: function (res) {
            $('#editUserModal .modal-body').html(res);
        }
    });
});


// AJAX for adding new lead modal

$(document).on('click', '#addNewLeadButton', function () {
    $.ajax({
        type: 'GET',
        url: '/leads/create-form',
        success: function (res) {
            $('#addNewLeadModal .modal-body').html(res);
        }
    })
});

// AJAX for editing lead modal

$(document).on('click', '.editLeadButton', function () {
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url: '/leads/' + leadId + '/edit-form',
        success: function (res) {
            $('#editLeadModal .modal-body').html(res);
        }
    });
});

// AJAX for assigning salesperson modal

$(document).on('click', '.update-sales', function(){
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url: '/get-sales/' + leadId,
        success: function (res) {
            $('#assignModal .modal-body').html(res);
        }
    });
});

// AJAX for creating new call

$(document).on('click', '.callLeadButton', function () {
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url:'/calls/create-form/' + leadId,
        success:  function (res) {
            $('#callLeadModal .modal-body').html(res);
        }
    });
});

