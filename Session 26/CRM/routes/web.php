<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth', 'Admin']], function () {

    // Lead Routes

    Route::get('leads', 'LeadController@index')->name('leads.index');
    Route::get('leads/create', 'LeadController@create')->name('leads.create');
    Route::post('leads', 'LeadController@store')->name('leads.store');
    Route::get('leads/{lead}/edit', 'LeadController@edit')->name('leads.edit');
    Route::get('leads/{lead}', 'LeadController@show')->where(['lead' => '[0-9]+'])->name('leads.show');
    Route::put('leads/update', 'LeadController@update')->name('leads.update');
    Route::delete('leads/{lead}', 'LeadController@destroy')->name('leads.destroy');
    Route::get('leads/create-form', 'LeadController@createForm')->name('leads.create-form');
    Route::get('leads/{lead}/edit-form', 'LeadController@editForm')->name('leads.edit-form');
    Route::post('leads/assign', 'LeadController@assign')->name('leads.assign');

    // User Routes

    Route::resource('users', 'UserController');

    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users', 'UserController@store')->name('users.store');
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('users/update', 'UserController@update')->name('users.update');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
    Route::get('users/create-form', 'UserController@createForm')->name('users.create-form');
    Route::get('users/{user}/edit-form', 'UserController@editForm')->name('users.edit-form');

    Route::get('calls', 'CallController@index')->name('calls.index');
    Route::get('calls/{call}', 'CallController@show')->where(['call' => '[0-9]+'])->name('calls.show');

});

Route::group(['middleware' => ['auth', 'TeamLeader']], function () {
    Route::get('leads', 'LeadController@index')->name('leads.index');
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('leads/{lead}', 'LeadController@show')->where(['lead' => '[0-9]+'])->name('leads.show');
    Route::get('users/{user}', 'UserController@show')->where(['user' => '[0-9]+'])->name('users.show');
    Route::get('calls', 'CallController@index')->name('calls.index');
    Route::get('calls/{call}', 'CallController@show')->where(['call' => '[0-9]+'])->name('calls.show');
    Route::post('leads/assign', 'LeadController@assign')->name('leads.assign');
});

Route::group(['middleware' => ['auth', 'Salesperson']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/leads', 'LeadController@index')->name('leads.index');
    Route::get('calls', 'CallController@index')->name('calls.index');
    Route::get('calls/create/{lead}', 'CallController@create')->name('calls.create');
    Route::get('calls/create-form/{lead}', 'CallController@createForm')->name('calls.create-form');
    Route::post('calls', 'CallController@store')->name('calls.store');
    Route::get('calls/{call}', 'CallController@show')->name('calls.show');
});

Route::get('/get-sales/{lead}', 'LeadController@updateSalesByLeadId');

