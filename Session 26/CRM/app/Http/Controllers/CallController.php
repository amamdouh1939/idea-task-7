<?php

namespace App\Http\Controllers;

use App\Call;
use App\Lead;
use App\LeadStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role->slug == 'SALES'){
            $calls = Call::where('user_id', Auth::user()->id)->get();

            return view('backoffice.calls.index', ["calls" => $calls]);
        }

        $calls = Call::all();

        return view('backoffice.calls.index', ["calls" => $calls]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Lead $lead)
    {
        $calls = Call::where('lead_id', $lead->id)->get();
        $userId = Auth::user()->id;
        $statuses = LeadStatus::all();

        return view('backoffice.calls.create', ["user_id" => $userId, "lead" => $lead, "calls" => $calls, "statuses" => $statuses]);
    }

    public function createForm(Lead $lead)
    {
        $calls = Call::where('lead_id', $lead->id)->get();
        $userId = Auth::user()->id;
        $statuses = LeadStatus::all();

        return view('backoffice.calls.create-form', ["user_id" => $userId, "lead" => $lead, "calls" => $calls, "statuses" => $statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $call = new Call();

        $call->user_id = Auth::user()->id;
        $call->lead_id = $request->input('lead_id');
        $call->call_log = $request->input('call_log');
        $call->status_id = $request->input('status_id');

        $call->save();

        return redirect('/leads');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function show(Call $call)
    {
        return view('backoffice.calls.show', ["call" => $call]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function edit(Call $call)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Call $call)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call $call)
    {
        //
    }
}
