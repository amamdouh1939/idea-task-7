<?php

namespace App\Http\Controllers\API;

use App\Lead;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LeadController extends Controller
{
    public function index()
    {
        $leads = Lead::where('status', 1)->paginate(5);

        return $leads;
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => 'required|digits:11|unique:leads,phone',
            'email' => 'email|unique:leads,email',
            'address' => 'required|string'
        ]);

        if($validator->fails()){
            return [
                'status' => 405,
                'errors' => $validator->errors()
            ];
        }

        $lead = new Lead();
        $lead->name = $request->input('name');
        $lead->phone = $request->input('phone');
        $lead->email = $request->input('email');
        $lead->address = $request->input('address');

        $lead->save();

        return $lead;

    }

    public function show(Lead $lead)
    {
        return $lead;
    }

    public function update(Request $request, Lead $lead)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => [
                'required', 'digits:11',
                Rule::unique('leads')->ignore($lead)
            ],
            'email' => [
                'email',
                Rule::unique('leads')->ignore($lead)
            ],
            'address' => 'required|string'
        ]);

        if($validator->fails()){
            return [
                'status' => 405,
                'errors' => $validator->errors()
            ];
        }

        $lead->name = $request->input('name');
        $lead->phone = $request->input('phone');
        $lead->email = $request->input('email');
        $lead->address = $request->input('address');

        $lead->save();

        return $lead;
    }

    public function destroy(Lead $lead)
    {
        $lead->status = false;

        $lead->save();

        return Lead::where('status', 1)->get();
    }
}
