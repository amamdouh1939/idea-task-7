@extends('layouts.app')

@section('title', 'Calls')

@section('pagename', 'View All Calls')

@section('content')

@if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR")
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Lead</th>
                <th>User</th>
                <th>Date</th>
                <th>Status</th>
                <th>View Call</th>
            </tr>
        </thead>
        <tbody>
            @foreach($calls as $call)
                <tr>
                    <td>{{ $call->lead->name }}</td>
                    <td>{{ $call->user->name }}</td>
                    <td>{{ $call->created_at }}</td>
                    <td>{{ $call->leadStatus->status }}</td>
                    <td>
                        <a href="{{ route('calls.show', $call) }}" class="btn btn-primary">Show</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@elseif(Auth::user()->role->slug == "SALES")
    @if(sizeof($calls) > 0)
        <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Lead</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>View Call</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($calls as $call)
                    @if($call->user_id == Auth::user()->id)
                        <tr>
                            <td>{{ $call->lead->name }}</td>
                            <td>{{ $call->created_at }}</td>
                            <td>{{ $call->leadStatus->status }}</td>
                            <td>
                                <a href="{{ route('calls.show', $call) }}" class="btn btn-primary">Show</a>
                            </td>
                        </tr>
                    @endif
                    @endforeach
                </tbody>
        </table>
    @else
        <h3><strong>{{ Auth::user()->name }} hasn't made any calls.</strong></h3>
    @endif
@endif

@endsection
