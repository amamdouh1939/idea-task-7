@extends('layouts.app')

@section('title', 'Create Call')

@section('pagename', 'Create Call')

@section('content')
    <h2><strong>Lead Name: </strong>{{ $lead->name }}</h2>
    <hr>
    <form action="{{ route('calls.store') }}" method="POST">
        @csrf
        <input type="hidden" name="lead_id" value="{{ $lead->id }}">
        <div class="form-group">
            <label for="call-log">Call Log</label>
            <textarea name="call_log" id="call-log" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="lead-status">Lead Status</label>
            <select name="status_id" id="lead-status" class="form-control">
                @foreach($statuses as $status)
                    <option value="{{ $status->id }}">{{ $status->status }}</option>
                @endforeach
            </select>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success">Save Call</button>
        </div>
    </form>
    <hr>
    @if(sizeof($calls) > 0)
        <h3>Call History</h3>
        <table class="table table-bordered">
            <thead>
                <th>Date</th>
                <th>User</th>
                <th>Lead Status</th>
                <th>Call Log</th>
            </thead>
            <tbody>
                @foreach($calls as $call)
                    <tr>
                        <td>{{ $call->created_at }}</td>
                        <td>{{ $call->user->name }}</td>
                        <td>{{ $call->leadStatus->status }}</td>
                        <td>{{ $call->call_log }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <h3><strong>There is no call history for {{ $lead->name }}.</strong></h3>
    @endif
@endsection
