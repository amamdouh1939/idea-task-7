@extends('layouts.app')

@section('title', 'Leads')
@section('pagename', 'View All Leads')

@section('content')
@if(Auth::user()->role->slug === "ADMIN")
    <button type="button" id="addNewLeadButton" class="btn btn-success" data-toggle="modal" data-target="#addNewLeadModal">Add New Lead</button>
    <hr>
@endif

@if(Auth::user()->role->slug == 'ADMIN' || Auth::user()->role->slug == "TMLDR")
<table class="table table-bordered">
    <thead>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Salesperson</th>
        <th>Actions</th>
    </thead>
        <tbody>
            @foreach($leads as $lead)
                <tr>
                    <td>{{ $lead->name }}</td>
                    <td>{{ $lead->phone }}</td>
                    <td>{{ $lead->email }}</td>
                    <td>{{ isset($lead->sales)? $lead->sales->name : 'Unassigned' }}</td>
                    <td>
                        <a href="{{ route('leads.show', $lead) }}" class="btn btn-primary">Show</a>
                        <button type="button" class="btn btn-info update-sales" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#assignModal">Assign</button>
                        @if(Auth::user()->role->slug === "ADMIN")
                            <button type="button" lead-id="{{ $lead->id }}" class="btn btn-warning editLeadButton" data-toggle="modal" data-target="#editLeadModal">Edit</button>
                        @endif

                        @if(Auth::user()->role->slug == "ADMIN")
                            <form style="display: inline-block" action="{{ route('leads.destroy', $lead) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        @endif
                        @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                            <button type="button" class="btn btn-success callLeadButton" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#callLeadModal">Call</button>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
        {{ $leads->links() }}
</table>
@elseif(Auth::user()->role->slug == "SALES")
@if(sizeof(Auth::user()->leads) > 0)
    <table class="table table-bordered">
        <thead>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Actions</th>
        </thead>
            <tbody>
                @foreach(Auth::user()->leads as $lead)
                    <tr>
                        <td>{{ $lead->name }}</td>
                        <td>{{ $lead->phone }}</td>
                        <td>{{ $lead->email }}</td>
                        <td>
                            @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                                <button type="button" class="btn btn-success callLeadButton" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#callLeadModal">Call</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
            {{ $leads->links() }}
    </table>

@else
    <h3><strong>There are no assigned leads for {{ Auth::user()->name }}.</strong></h3>
@endif
@endif

<!-- Adding New Lead Modal -->

<div class="modal fade" id="addNewLeadModal" tabindex="-1" role="dialog" aria-labelledby="addNewLeadModalLabel" aria-hidden="true">
    <form action="{{ route('leads.store') }}" method="POST">
    @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewLeadModalLabel">Add New Lead</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
    </form>
</div>

<!-- Edit Lead Modal -->

<div class="modal fade" id="editLeadModal" tabindex="-1" role="dialog" aria-labelledby="editLeadModalLabel" aria-hidden="true">
    <form action="{{ route('leads.update') }}" method="POST">
    @csrf
    @method('PUT')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editLeadModalLabel">Edit Lead</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
    </form>
</div>

<!-- Assigning Salesperson Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="assignModalLabel" aria-hidden="true">
    <form action="{{ route('leads.assign') }}" method="POST">
        @csrf
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="assignModalLabel">Assign Salesperson to Lead</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success">Assign</button>
            </div>
          </div>
        </div>
    </form>
</div>

<!-- Create Call Modal -->

<div class="modal fade" id="callLeadModal" tabindex="-1" role="dialog" aria-labelledby="callLeadModalLabel" aria-hidden="true">
    <form action="{{ route('calls.store') }}" method="POST">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="callLeadModalLabel">Create Call</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
    </form>
</div>
@endsection
