@extends('layouts.app')

@section('title', 'Edit Lead')

@section('pagename', 'Edit Lead')

@section('content')
<form action="{{ route('leads.update', $lead) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ $lead->name }}">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="tel" name="phone" id="phone" class="form-control" value="{{ $lead->phone }}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ $lead->email }}">
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" name="address" id="address" class="form-control" value="{{ $lead->address }}">
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success">Save</button>
    </div>
</form>

@endsection
