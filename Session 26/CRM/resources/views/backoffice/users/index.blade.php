@extends('layouts.app')

@section('title', 'Users')
@section('pagename', 'View All Users')

@section('content')
    @if(Auth::user()->role->slug == "ADMIN")
        <button type="button" id="addNewUserButton" class="btn btn-success" data-toggle="modal" data-target="#addNewUserModal">Add New User</button>
        <hr>
    @endif
    <table class="table table-bordered">
        <thead>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->title }}</td>
                    <td>
                        <a href="{{ route('users.show', $user) }}" class="btn btn-primary">Show</a>
                        @if(Auth::user()->role->slug == "ADMIN")
                            <button type="button" class="btn btn-warning editUserButton" user-id="{{ $user->id }}" data-toggle="modal" data-target="#editUserModal">Edit</button>
                        @endif
                        @if(Auth::user()->role->slug == "ADMIN")
                            <form style="display: inline-block" action="{{ route('users.destroy', $user) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>


<!-- Adding New User Modal -->

<div class="modal fade" id="addNewUserModal" tabindex="-1" role="dialog" aria-labelledby="addNewUserModalLabel" aria-hidden="true">
    <form action="{{ route('users.store') }}" method="POST">
    @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewUserModalLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
    </form>
</div>

<!-- Edit User Modal -->

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="editUserModalLabel" aria-hidden="true">
    <form action="{{ route('users.update') }}" method="POST">
    @csrf
    @method('PUT')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserModalLabel">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            </div>
        </div>
    </form>
</div>
@endsection
