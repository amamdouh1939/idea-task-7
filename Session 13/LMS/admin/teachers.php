<?php require_once 'tpl/header.php';
      require_once 'libs/Teacher.php';

      $t1 = new Teacher();
      $t1->id = 1;
      $t1->name = "Ahmed";
      $t1->email = "Ahmed@gmail.com";
      $t1->phone = "010005296753";

      $t2 = new Teacher();
      $t2->id = 2;
      $t2->name = "Ali";
      $t2->email = "ali@gmail.com";
      $t2->phone = "010002296753";

      $t3 = new Teacher();
      $t3->id = 3;
      $t3->name = "Mahmoud";
      $t3->email = "Mahmoud@gmail.com";
      $t3->phone = "010008896753";

      $teachers = [
          $t1,
          $t2,
          $t3
      ];

?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Teachers</h1>

                <div class="card">
                    <!-- Button trigger modal -->
                    <button type="button"
                            class="btn btn-success m-3"
                            data-toggle="modal"
                            data-target="#add-new">
                        Add New
                    </button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="teachersTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($teachers as $key => $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item->id ?></td>
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->email; ?></td>
                                        <td><?php echo $item->phone; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-info">Show</a>
                                            <a href="#" class="btn btn-primary">Edit</a>
                                            <a href="#" class="btn btn-danger" onclick="swalPopup()">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- Modal -->

<div class="modal fade" id="add-new" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">
                                    Name:
                                </label>
                                <input id="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input id="phone" type="tel" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="desc">Description</label>
                            <textarea id="desc"
                                      class="form-control"
                                      cols="30"
                                      rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>

        <!-- End of Main Content -->

<?php require_once 'tpl/footer.php' ?>