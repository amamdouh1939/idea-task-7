<?php require_once 'tpl/header.php';
      require_once 'libs/User.php';

      $u1 = new User();
      $u1->id = 1;
      $u1->name = "Ahmed";
      $u1->email = "ahmed@gmail.com";

      $u2 = new User();
      $u2->id = 2;
      $u2->name = "Mohammed";
      $u2->email = "mohammed@gmail.com";

      $u3 = new User();
      $u3->id = 3;
      $u3->name = "Ali";
      $u3->email = "ali@gmail.com";

      $u4 = new User();
      $u4->id = 4;
      $u4->name = "Hossam";
      $u4->email = "hossam@gmail.com";

      $u5 = new User();
      $u5->id = 5;
      $u5->name = "Mahmoud";
      $u5->email = "mahmoud@gmail.com";

      $users = [
          $u1,
          $u2,
          $u3,
          $u4,
          $u5
      ];

?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Users</h1>

                <div class="card">
                    <button type="button"
                            class="btn btn-success m-3"
                            data-toggle="modal"
                            data-target="#add-new">
                        Add New
                    </button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="usersTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($users as $key => $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item->id ?></td>
                                        <td><?php echo $item->name ?></td>
                                        <td><?php echo $item->email ?></td>
                                        <td>
                                            <a href="#" class="btn btn-info">Show</a>
                                            <a href="#" class="btn btn-primary">Edit</a>
                                            <a href="#" class="btn btn-danger" onclick="swalPopup()">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- Modal -->

<div class="modal fade" id="add-new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">
                                    Name
                                </label>
                                <input id="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>

        <!-- End of Main Content -->

    <?php require_once 'tpl/footer.php' ?>