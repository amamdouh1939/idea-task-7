<?php require_once 'tpl/header.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Course Content</h1>

                <form>
                    <div class="form-group" id="dynamic-form">
                        <div class="row">
                            <div class="col-md-6">
                                <input name="content" type="text" id="content" class="form-control">
                            </div>
                            <div class="button-group col-md-6">
                                <a href="javascript:void(0)" class="btn btn-primary" id="plus">Add</a>
                                <a href="javascript:void(0)" class="btn btn-danger" id="minus">Remove</a>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-success">
                </form>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"
	        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
	        crossorigin="anonymous"></script>

    <script src="js/dynamic-form.js"></script>

<script>
    var dynamic_form = $("#dynamic-form").dynamicForm("#dynamic-form", "#plus", "#minus", {
        limit: 10,
        formPrefix: "dynamic-form",
        normalizeFullForm: false,
	    data: {}
    });
</script>

</body>

</html>
