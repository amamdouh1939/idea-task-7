<?php require_once 'tpl/header.php';
      require_once 'libs/Course.php';

      $c1 = new Course();
      $c1->id = 1;
      $c1->name = 'Full-Stack';
      $c1->duration = 150;
      $c1->level = 1;

      $c2 = new Course();
      $c2->id = 2;
      $c2->name = 'MEAN-Stack';
      $c2->duration = 150;
      $c2->level = 1;

      $courses = [
          $c1,
          $c2
      ];


?>

<!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Courses</h1>

                <div class="card">
                    <!-- Button trigger modal -->
                    <button type="button"
                            class="btn btn-success m-3"
                            data-toggle="modal"
                            data-target="#add-new" id="add-new-button">
                        Add New
                    </button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="coursesTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Duration</th>
                                <th>Level</th>
                                <th>Pre-Quizzed</th>
                                <th>Content</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($courses as $key => $item) {
                                ?>
                                    <tr>
                                        <td><?php echo $item->id; ?></td>
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->duration; ?>h</td>
                                        <td>Level <?php echo $item->level; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-primary bg-secondary">Courses</a>
                                        </td>
                                        <td>
                                            <a href="course-content.html" class="btn btn-success">Content</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info">Show</a>
                                            <a href="#" class="btn btn-primary">Edit</a>
                                            <a href="#" class="btn btn-danger" onclick="swalPopup()">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Modal -->

<div class="modal fade" id="add-new" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">
                                    Name:
                                </label>
                                <input id="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="duration">Duration</label>
                                <input id="duration"
                                       type="number"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select id="level" class="form-control select-menu" name="level">
                                    <option value="1">Level 1</option>
                                    <option value="2">Level 2</option>
                                    <option value="3">Level 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="courses">Courses</label>
                                <select id="courses" class="form-control select-menu" multiple>
                                    <option value="1">Course 1</option>
                                    <option value="2">Course 2</option>
                                    <option value="3">Course 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="desc">Description</label>
                            <textarea id="desc"
                                      class="form-control"
                                      cols="30"
                                      rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>

    <?php require_once 'tpl/footer.php' ?>