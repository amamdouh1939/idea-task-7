<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth', 'Admin']], function () {
    Route::resource('leads', 'LeadController');
    Route::resource('users', 'UserController');
    Route::put('leads/{lead}/assign', 'LeadController@assign')->name('leads.assign');
});

Route::group(['middleware' => ['auth', 'TeamLeader']], function () {
    Route::get('leads', 'LeadController@index')->name('leads.index');
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('leads/{lead}', 'LeadController@show')->name('leads.show');
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::put('leads/{lead}/assign', 'LeadController@assign')->name('leads.assign');
});

Route::group(['middleware' => ['auth', 'Salesperson']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

