// ==============================================

//   U      S       E       R       S

// ==============================================

// AJAX for getting users table

function getAllUsers(){
    $.ajax({
        type: 'GET',
        url: '/users/ajax/table',
        success: function(res){
            $('#usersTable').html(res);
        }
    });
}


// AJAX for adding new user modal
// GET

$(document).on('click', '#addNewUserButton', function () {
    $.ajax({
        type: 'GET',
        url: '/users/create-form',
        success: function (res) {
            $('#addNewUserModal .modal-body').html(res);
        }
    });
});

// POST
$(document).on('submit', '#addNewUserModal form', function (e) {
    e.preventDefault();
    let form = new FormData($(this)[0]);

    $.ajax({
        type: 'POST',
        url: '/users',
        data: form,
        contentType: false,
        processData: false,
        success: function(res){
            getAllUsers();
            $('#addNewUserModal').modal('hide');
        },

        error: function(res){
            const values = Object.values(res.responseJSON.errors);
            let error = `<ul>`;
            for(let i = 0; i < values.length; i++){
                error += `<li>${values[i][0]}</li>`;
                console.log(values[i][0]);
            }
            error += `</li>`;

            $('#addNewUserModal .form-error').html(error);
        }
    });
});

// AJAX for editing user modal
// GET

$(document).on('click', '.editUserButton', function () {
    const userId = $(this).attr('user-id');

    $.ajax({
        type: 'GET',
        url: '/users/' + userId + '/edit-form',
        success: function (res) {
            $('#editUserModal .modal-body').html(res);
        }
    });
});

// POST

$(document).on('submit', '#editUserModal form', function (e) {
    e.preventDefault();
    let form = new FormData($(this)[0]);

    $.ajax({
        type: 'POST',
        url: '/users/update',
        data: form,
        processData: false,
        contentType: false,
        success: function(res){
            getAllUsers();
            $('#editUserModal').modal('hide');
        },
        error: function(res){
            const values = Object.values(res.responseJSON.errors);
            let error = `<ul>`;
            for(let i = 0; i < values.length; i++){
                error += `<li>${values[i][0]}</li>`;
                console.log(values[i][0]);
            }
            error += `</li>`;

            $('#editUserModal .form-error').html(error);
        }
    });
});


// ==========================================================================

// ==========================================================================

// ==========================================================================

// ==============================================

//   L      E       A       D       S

// ==============================================


// AJAX for getting leads table

function getAllLeads(){
    $.ajax({
        type: 'GET',
        url: '/leads/ajax/table',
        success: function (res) {
            $('#leadsTable').html(res);
        }
    })
}


// AJAX for adding new lead modal
// GET

$(document).on('click', '#addNewLeadButton', function () {
    $.ajax({
        type: 'GET',
        url: '/leads/create-form',
        success: function (res) {
            $('#addNewLeadModal .modal-body').html(res);
        }
    })
});

// POST
$(document).on('submit', '#addNewLeadModal form', function (e) {
    e.preventDefault();
    let form = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: '/leads',
        processData: false,
        contentType: false,
        data: form,
        success: function(res){
            getAllLeads();
            $('#addNewLeadModal').modal('hide');
        },

        error: function(res){
            const values = Object.values(res.responseJSON.errors);
            let error = `<ul>`;
            for(let i = 0; i < values.length; i++){
                error += `<li>${values[i][0]}</li>`;
                console.log(values[i][0]);
            }
            error += `</li>`;

            $('#addNewLeadModal .form-error').html(error);
        }

    });
});

// AJAX for editing lead modal
// GET

$(document).on('click', '.editLeadButton', function () {
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url: '/leads/' + leadId + '/edit-form',
        success: function (res) {
            $('#editLeadModal .modal-body').html(res);
        }
    });
});

//POST
$(document).on('submit', '#editLeadModal form', function (e){
    e.preventDefault();
    const form = new FormData($(this)[0]);

    $.ajax({
        type: 'POST',
        url: '/leads/update',
        data: form,
        processData: false,
        contentType: false,
        success: function(res){
            if(res.status == 200){
                getAllLeads();
                $('#editLeadModal').modal('hide');
            }
        },
        error: function(res){
            const values = Object.values(res.responseJSON.errors);
            let error = `<ul>`;
            for(let i = 0; i < values.length; i++){
                error += `<li>${values[i][0]}</li>`;
                console.log(values[i][0]);
            }
            error += `</li>`;

            $('#editLeadModal .form-error').html(error);
        }
    })
});

// AJAX for assigning salesperson modal
// GET

$(document).on('click', '.update-sales', function(){
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url: '/get-sales/' + leadId,
        success: function (res) {
            $('#assignModal .modal-body').html(res);
        }
    });
});


// ==========================================================================

// ==========================================================================

// ==========================================================================

// ==============================================

//   C      A       L       L       S

// ==============================================


// AJAX for creating new call
// GET

$(document).on('click', '.callLeadButton', function () {
    const leadId = $(this).attr('lead-id');

    $.ajax({
        type: 'GET',
        url:'/calls/create-form/' + leadId,
        success:  function (res) {
            $('#callLeadModal .modal-body').html(res);
        }
    });
});

