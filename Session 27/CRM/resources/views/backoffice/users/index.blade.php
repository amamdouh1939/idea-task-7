@extends('layouts.app')
@section('title', 'Users')
@section('pagename', 'View All Users')
@section('content')

@if(Auth::user()->role->slug == "ADMIN")
    <button
        type="button"
        id="addNewUserButton"
        class="btn btn-success"
        data-toggle="modal"
        data-target="#addNewUserModal"
    >
        Add New User
    </button>
    <hr />
@endif

<div id="usersTable">
    {!! $table !!}
</div>

<!-- Adding New User Modal -->

<div
    class="modal fade"
    id="addNewUserModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="addNewUserModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewUserModalLabel">
                    Add New User
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-error"></div>
            <form action="{{ route('users.store') }}" method="POST">
                @csrf
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit User Modal -->

<div
    class="modal fade"
    id="editUserModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="editUserModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserModalLabel">
                    Edit User
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-error"></div>
            <form action="{{ route('users.update') }}" method="POST">
                @csrf @method('PUT')
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
