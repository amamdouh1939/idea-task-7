@extends('layouts.app')

@section('title', 'Edit User')

@section('pagename', 'Edit User')

@section('content')
<form action="{{ route('users.update', $user) }}" method="POST">
    @csrf
    @method('PUT')

    @include('backoffice.users.edit-form')

    <div class="text-center">
        <button type="submit" class="btn btn-success">Save</button>
    </div>
</form>

@endsection
