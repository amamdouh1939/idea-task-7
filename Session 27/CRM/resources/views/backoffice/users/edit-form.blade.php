<input type="hidden" name="user" value="{{ $user->id }}">

<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}">
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" class="form-control">
</div>
<div class="form-group">
    <label for="role">Role</label>
    <select name="role" id="role" class="form-control">
        @foreach($roles as $role)
            @if($user->role->id == $role->id)
                <option value="{{ $role->id }}" selected>{{ $role->title }}</option>
            @else
                <option value="{{ $role->id }}">{{ $role->title }}</option>
            @endif
        @endforeach
    </select>
</div>
