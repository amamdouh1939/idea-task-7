@extends('layouts.app')

@section('title', 'Create User')

@section('pagename', 'Create New User')

@section('content')
    <form action="{{ route('users.store') }}" method="POST">
        @csrf
        @include('backoffice.users.create-form')
        <div class="text-center">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
@endsection
