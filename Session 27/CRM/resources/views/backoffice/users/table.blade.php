<table class="table table-bordered">
    <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role->title }}</td>
                <td>
                    <a href="{{ route('users.show', $user) }}" class="btn btn-primary">Show</a>
                    @if(Auth::user()->role->slug == "ADMIN")
                        <button type="button" class="btn btn-warning editUserButton" user-id="{{ $user->id }}" data-toggle="modal" data-target="#editUserModal">Edit</button>
                    @endif
                    @if(Auth::user()->role->slug == "ADMIN")
                        <form style="display: inline-block" action="{{ route('users.destroy', $user) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        {{ $users->links() }}
    </tbody>
</table>
