@extends('layouts.app')

@section('title', 'Leads')

@section('pagename', 'View All Leads')

@section('content')

@if(Auth::user()->role->slug === "ADMIN")
    <button
        type="button"
        id="addNewLeadButton"
        class="btn btn-success"
        data-toggle="modal"
        data-target="#addNewLeadModal"
    >
        Add New Lead
    </button>
    <hr />
@endif

<div id="leadsTable">
    {!! $table !!}
</div>

<!-- Adding New Lead Modal -->

<div
    class="modal fade"
    id="addNewLeadModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="addNewLeadModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewLeadModalLabel">
                    Add New Lead
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-error"></div>
            <form action="{{ route('leads.store') }}" method="POST">
                @csrf
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Lead Modal -->

<div
    class="modal fade"
    id="editLeadModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="editLeadModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editLeadModalLabel">Edit Lead</h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-error"></div>
            <form action="{{ route('leads.update') }}" method="POST">
                @csrf @method('PUT')

                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Assigning Salesperson Modal -->
<div
    class="modal fade"
    id="assignModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="assignModalLabel"
    aria-hidden="true"
>
    <form action="{{ route('leads.assign') }}" method="POST">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="assignModalLabel">
                        Assign Salesperson to Lead
                    </h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">
                        Assign
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Create Call Modal -->

<div
    class="modal fade"
    id="callLeadModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="callLeadModalLabel"
    aria-hidden="true"
>
    <form action="{{ route('calls.store') }}" method="POST">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="callLeadModalLabel">
                        Create Call
                    </h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
