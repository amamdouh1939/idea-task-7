
    <input type="hidden" name="lead" value="{{ $lead->id }}">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ $lead->name }}">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="tel" name="phone" id="phone" class="form-control" value="{{ $lead->phone }}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ $lead->email }}">
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" name="address" id="address" class="form-control" value="{{ $lead->address }}">
    </div>
