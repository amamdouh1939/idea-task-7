@extends('layouts.app')

@section('title', 'Create Lead')

@section('pagename', 'Create New Lead')

@section('content')
<form action="{{ route('leads.store') }}" method="POST">
    @csrf
    @include('backoffice.leads.create-form')
    <div class="text-center">
        <button type="submit" class="btn btn-success">Save</button>
    </div>
</form>
@endsection
