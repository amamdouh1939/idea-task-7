<input type="hidden" name="lead" value="{{ $lead->id }}">

<label for="sales">Salesperson</label>
    <br>
    <select name="sales" id="sales">
        <option value="0">Unassigned</option>
        @foreach($sales as $salesperson)
            @if($salesperson->id == $lead->sales_id)
                <option value="{{ $salesperson->id }}" selected>{{ $salesperson->name }}</option>
            @else
                <option value="{{ $salesperson->id }}">{{ $salesperson->name }}</option>
            @endif
        @endforeach
    </select>
</form>
