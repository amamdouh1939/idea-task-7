@if(Auth::user()->role->slug == 'ADMIN' || Auth::user()->role->slug == "TMLDR")
    <table class="table table-bordered">
        <thead>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Salesperson</th>
            <th>Actions</th>
        </thead>
            <tbody>
                @foreach($leads as $lead)
                    <tr>
                        <td>{{ $lead->name }}</td>
                        <td>{{ $lead->phone }}</td>
                        <td>{{ $lead->email }}</td>
                        <td>{{ isset($lead->sales)? $lead->sales->name : 'Unassigned' }}</td>
                        <td>
                            <a href="{{ route('leads.show', $lead) }}" class="btn btn-primary">Show</a>
                            <button type="button" class="btn btn-info update-sales" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#assignModal">Assign</button>
                            @if(Auth::user()->role->slug === "ADMIN")
                                <button type="button" lead-id="{{ $lead->id }}" class="btn btn-warning editLeadButton" data-toggle="modal" data-target="#editLeadModal">Edit</button>
                            @endif

                            @if(Auth::user()->role->slug == "ADMIN")
                                <form style="display: inline-block" action="{{ route('leads.destroy', $lead) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            @endif
                            @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                                <button type="button" class="btn btn-success callLeadButton" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#callLeadModal">Call</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
            {{ $leads->links() }}
    </table>
@elseif(Auth::user()->role->slug == "SALES")
    @if(sizeof(Auth::user()->leads) > 0)
        <table class="table table-bordered">
            <thead>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Actions</th>
            </thead>
                <tbody>
                    @foreach(Auth::user()->leads as $lead)
                        <tr>
                            <td>{{ $lead->name }}</td>
                            <td>{{ $lead->phone }}</td>
                            <td>{{ $lead->email }}</td>
                            <td>
                                @if(Auth::user()->role->slug == "ADMIN" || Auth::user()->role->slug == "TMLDR" || Auth::user()->role->slug == "SALES")
                                    <button type="button" class="btn btn-success callLeadButton" lead-id="{{ $lead->id }}" data-toggle="modal" data-target="#callLeadModal">Call</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                {{ $leads->links() }}
        </table>

    @else
        <h3><strong>There are no assigned leads for {{ Auth::user()->name }}.</strong></h3>
    @endif
@endif
