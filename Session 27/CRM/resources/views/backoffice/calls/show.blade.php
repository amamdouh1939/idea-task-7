@extends('layouts.app')

@section('title', 'Show Call')

@section('pagename', 'Show Call')

@section('content')

<h3><strong>User Name:</strong> {{ $call->user->name }}</h3>
<h3><strong>Lead Name:</strong> {{ $call->lead->name }}</h3>
<h3><strong>Call Time:</strong> {{ $call->created_at }}</h3>
<h3><strong>Lead Status:</strong> {{ $call->leadStatus->status }}</h3>
<hr>
<h4><strong>Call Log</strong></h4>
<p>{{ $call->call_log }}</p>

@endsection
