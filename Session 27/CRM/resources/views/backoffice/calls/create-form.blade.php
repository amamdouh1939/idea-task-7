<h2><strong>Lead Name: </strong>{{ $lead->name }}</h2>
<input type="hidden" name="lead_id" value="{{ $lead->id }}">
<div class="form-group">
    <label for="call-log">Call Log</label>
    <textarea name="call_log" id="call-log" cols="30" rows="10" class="form-control"></textarea>
</div>
<div class="form-group">
    <label for="lead-status">Lead Status</label>
    <select name="status_id" id="lead-status" class="form-control">
        @foreach($statuses as $status)
            <option value="{{ $status->id }}">{{ $status->status }}</option>
        @endforeach
    </select>
</div>
