<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    public function sales()
    {
        return $this->belongsTo(User::class, 'sales_id');
    }

    public function calls()
    {
        return $this->hasMany(Calls::class, 'lead_id');
    }
}
