<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Salesperson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role->slug !== 'ADMIN'
            && Auth::user()->role->slug !== 'TMLDR'
            && Auth::user()->role->slug !== 'SALES'){
                return \abort(403);
            }

        return $next($request);
    }
}
