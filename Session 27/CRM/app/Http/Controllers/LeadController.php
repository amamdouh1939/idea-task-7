<?php

namespace App\Http\Controllers;

use App\Lead;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LeadRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $table = view('backoffice.leads.table', [
            'leads' => Lead::where('status', 1)->paginate(5),
            'sales' => User::getAllSales()
        ]);
        return view('backoffice.leads.index', ["table" => $table]);
    }

    public function table()
    {
        return view('backoffice.leads.table', ["leads" => Lead::where('status', 1)->paginate(5)->withPath('/leads')]);
    }

    public function assign(Request $request)
    {
        $lead = Lead::find($request->input('lead'));

        if($request->input('sales') == 0){
            $lead->sales_id = null;
        } else {
            $lead->sales_id = $request->input('sales');
        }

        $lead->save();

        return redirect()->route('leads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.leads.create');
    }

    public function createForm()
    {
        return view('backoffice.leads.create-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        $lead = new Lead();

        $lead->name = $request->input('name');
        $lead->phone = $request->input('phone');
        $lead->email = $request->input('email');
        $lead->address = $request->input('address');

        if($lead->save()){
            return [
                "status" => 200,
                "message" => 'success'
            ];
        } else {
            return [
                "status" => 400,
                "message" => 'failure'
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        return view('backoffice.leads.show', ["lead" => $lead]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        return view('backoffice.leads.edit', ["lead" => $lead]);
    }

    public function editForm(Lead $lead)
    {
        return view('backoffice.leads.edit-form', ["lead" => $lead]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $lead = Lead::find($request->input('lead'));
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => [
                'required',
                Rule::unique('leads')->ignore($lead)
            ],
            'email' => [
                'email',
                Rule::unique('leads')->ignore($lead)
            ],
            'address' => 'required',
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $returnMessage = [
                'status' => 403,
                'errors' => $errors
            ];

            return response()->json($returnMessage, 403);
        }

        $lead->name = $request->input('name');
        $lead->phone = $request->input('phone');
        $lead->email = $request->input('email');
        $lead->address = $request->input('address');

        if($lead->save()){
            return [
                "status" => 200,
                "message" => "Updated"
            ];
        } else {
            return [
                "status" => 400,
                "message" => "Update Failed"
            ];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        $lead->status = false;

        $lead->save();
        return redirect()->route('leads.index');
    }

    public function updateSalesByLeadId(Lead $lead)
    {
        return view('backoffice.leads.sales-options', [
            'sales' => User::getAllSales(),
            'lead' => $lead
        ]);
    }
}
