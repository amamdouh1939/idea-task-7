<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Response;

/*

Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

*/

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.users.index', [
            "table" => view('backoffice.users.table', [
                "users" => User::where('status', 1)->paginate(5)->withPath('/users')
                ])
            ]);
    }

    public function table()
    {
        return view('backoffice.users.table', ["users" => User::where('status', 1)->paginate(5)->withPath('/users')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('backoffice.users.create', ["roles" => $roles]);
    }

    public function createForm()
    {
        $roles = Role::all();

        return view('backoffice.users.create-form', ["roles" => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'role' => ['required']
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $returnMessage = [
                "status" => 403,
                "message" => "failed",
                "errors" => $errors
            ];

            return response()->json($returnMessage, 403);

        }

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->role_id = $request->input('role');

        if($user->save()){
            $returnMessage = [
                "status" => 200,
                "message" => "OK"
            ];
        } else {

            return response()->json($returnMessage, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('backoffice.users.show', ["user" => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        return view('backoffice.users.edit', ["user" => $user, "roles" => $roles]);
    }

    public function editForm(User $user)
    {
        $roles = Role::all();
        return view('backoffice.users.edit-form', ["user" => $user, "roles" => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->input('user'));
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user)],
            'password' => ['required', 'string', 'min:8'],
            'role' => ['required']
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $returnMessage = [
                "status" => 403,
                "message" => "failed",
                "errors" => $errors
            ];

            return response()->json($returnMessage, 403);
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->role_id = $request->input('role');

        if($user->save()){
            $returnMessage = [
                "status" => 200,
                "message" => "OK"
            ];
        } else {

            return response()->json($returnMessage, 200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->status = false;
        $user->save();

        return redirect()->route('users.index');
    }
}
