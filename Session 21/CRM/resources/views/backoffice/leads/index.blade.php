@extends('layouts.app')

@section('title', 'Leads')
@section('pagename', 'View All Leads')

@section('content')
<a href="{{ route('leads.create') }}" class="btn btn-success">Add New</a>
<hr>
<table class="table table-bordered">
    <thead>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($leads as $lead)
            <tr>
                <td>{{ $lead->name }}</td>
                <td>{{ $lead->phone }}</td>
                <td>{{ $lead->email }}</td>
                <td>
                    <a href="{{ route('leads.show', $lead) }}" class="btn btn-primary">Show</a>
                    <a href="{{ route('leads.edit', $lead)}}" class="btn btn-warning">Edit</a>
                    <form style="display: inline-block" action="{{ route('leads.destroy', $lead) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
