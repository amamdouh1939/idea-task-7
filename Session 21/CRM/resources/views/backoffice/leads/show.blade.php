@extends('layouts.app')

@section('title', 'Show Lead')

@section('pagename', 'Show Lead')

@section('content')
    <h2>More information about {{ $lead->name }}</h2>
    <hr>
    <h3><strong>Name: </strong> {{ $lead->name }}</h3>
    <h3><strong>Phone: </strong>{{ $lead->phone }}</h3>
    <h3><strong>Email: </strong>{{ $lead->email }}</h3>
    <h3><strong>Address: </strong>{{ $lead->address }}</h3>
    <hr>
    <div>
        <a href="{{ route('leads.edit', $lead) }}" class="btn btn-warning">Edit</a>
        <form style="display: inline-block" action="{{ route('leads.destroy', $lead) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </div>

@endsection
