@extends('layouts.app')

@section('title', 'Users')
@section('pagename', 'View All Users')

@section('content')
    <a href="{{ route('users.create') }}" class="btn btn-success">Add New</a>
    <hr>
    <table class="table table-bordered">
        <thead>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->title }}</td>
                    <td>
                        <a href="{{ route('users.show', $user) }}" class="btn btn-primary">Show</a>
                        <a href="{{ route('users.edit', $user) }}" class="btn btn-warning">Edit</a>
                        <form style="display: inline-block" action="{{ route('users.destroy', $user) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
