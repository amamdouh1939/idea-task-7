@extends('layouts.app')

@section('title', 'Show User')

@section('pagename', 'Show User')

@section('content')
    <h2>More information about {{ $user->name }}</h2>
    <hr>
    <h3><strong>Name: </strong> {{ $user->name }}</h3>
    <h3><strong>Email: </strong>{{ $user->email }}</h3>
    <h3><strong>Role: </strong>{{ $user->role->title }}</h3>
    <hr>
    <div>
        <a href="{{ route('users.edit', $user) }}" class="btn btn-warning">Edit</a>
        <form style="display: inline-block" action="{{ route('users.destroy', $user) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </div>

@endsection
